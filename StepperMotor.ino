#include <math.h>

//konfiguracja PINów z A49988 (kontroler) -> Arduino UNO
#define DIR 6
#define STP 7
#define MS1 2
#define MS2 3
#define MS3 4

//czas trwania pełnego obrotu w sekundach
int revolutionTime = 30;
int stepsPerRevolution = 3200;
/*
 * Zmienna stepsPerRevolution pozwala wybrać jedną z kilku rozdzielczości obrotu silnika. Dostępne opcje:
 * 200, 400, 800, 1600, 3200
 * Na podstawie wybranej rozdzielczości program ustawia odpowiednią kombinację zmiennych MS1, MS2, MS3. Kolejno:
 * 200: LOW LOW LOW
 * 400: HIGH LOW LOW
 * 800: LOW HIGH LOW
 * 1600: HIGH HIGH LOW
 * 3200: HIGH HIGH HIGH
 * 
 * Dla wysokiej rozdzielczości interwał będzie bardzo mały, co powoduje
 * spadek maksymalnej prędkości obrotu (kontroler nie nadąża z wysyłaniem
 * impulsów z dużą częstotliwością). Dla małych prędkości należy wybierać
 * wysoką rozdzielczość, a dla dużych prędkości stosunkowo niską rozdzielczość
 */

//oblicza interwał czasowy pomiędzy wysłaniem kolejnych pulsów (krokiem silnika)
int stepInterval = round(((revolutionTime * 1000000) / stepsPerRevolution ) / 2);

void setup() {
  pinMode(DIR, OUTPUT);
  pinMode(STP, OUTPUT);
  digitalWrite(DIR, LOW);
  
  digitalWrite(MS1, HIGH);
  digitalWrite(MS2, HIGH);
  digitalWrite(MS3, HIGH);


  //NAPISANE DO DUPY. DO POPRAWKI - STWORZYĆ MAPĘ I PRZYPISAĆ ZA POMOCĄ INDEKSÓW
  if (stepsPerRevolution == 200) {
    digitalWrite(MS1, LOW);
    digitalWrite(MS2, LOW);
    digitalWrite(MS3, LOW);
  }
  else if (stepsPerRevolution == 400) {
    digitalWrite(MS1, HIGH);
    digitalWrite(MS2, LOW);
    digitalWrite(MS3, LOW);
  }
  else if (stepsPerRevolution == 800) {
    digitalWrite(MS1, LOW);
    digitalWrite(MS2, HIGH);
    digitalWrite(MS3, LOW);
  }
  else if (stepsPerRevolution == 1600) {
    digitalWrite(MS1, HIGH);
    digitalWrite(MS2, HIGH);
    digitalWrite(MS3, LOW);
  }
  else if (stepsPerRevolution == 3200) {
    digitalWrite(MS1, HIGH);
    digitalWrite(MS2, HIGH);
    digitalWrite(MS3, HIGH);
  }
  else {
    digitalWrite(MS1, LOW);
    digitalWrite(MS2, LOW);
    digitalWrite(MS3, LOW);
  }
}

void loop() {
  while(1) {
    digitalWrite(STP, HIGH); //wykonaj 1 krok
    delayMicroseconds(stepInterval);
    digitalWrite(STP, LOW); //przygotuj się do wykonania kolejnego kroku
    delayMicroseconds(stepInterval);
  }
 
}
